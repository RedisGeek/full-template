<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Tables</title>

    <!-- Bootstrap core CSS-->
    <link href="<c:url value='/static/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="<c:url value='/static/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="<c:url value='/static/vendor/datatables/dataTables.bootstrap4.css'/>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<c:url value='/static/css/sb-admin.css'/>" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="index.html">Start Bootstrap</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger">9+</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <span class="badge badge-danger">7</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">Settings</a>
            <a class="dropdown-item" href="#">Activity Log</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Login Screens:</h6>
            <a class="dropdown-item" href="login.html">Login</a>
            <a class="dropdown-item" href="register.html">Register</a>
            <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">Other Pages:</h6>
            <a class="dropdown-item" href="404.html">404 Page</a>
            <a class="dropdown-item" href="blank.html">Blank Page</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Tables</li>
          </ol>

<form>

  <div class = "panel panel-primary">
   <div class = "panel-heading">
      <h3 class = "panel-title">SAISIE</h3>
   </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="nom">NOM</label>
      <input type="text" class="form-control" id="nom" placeholder="NOM">
    </div>
    <div class="form-group col-md-6">
      <label for="prenom">PRENOM</label>
      <input type="text" class="form-control" id="prenom" placeholder="prenom">
    </div>
  </div>

  <div class="form-row">
 <div class="form-group col-md-4">
    <label for="numcin">NUMERO CIN</label>
    <input type="text" class="form-control" id="numcin" placeholder="NUMERO CIN">
  </div>

  <div class="form-group col-md-4">
    <label for="datedelv">DATE DE DELIVRANCE</label>
    <input type="date" class="form-control" id="datedelv" placeholder="DATE DE NAISSANCE">
  </div>

  <div class="form-group col-md-4">
    <label for="lieu">LIEU</label>
    <input type="text" class="form-control" id="lieu" placeholder="lieu">
  </div>
  </div>

  <div class="form-row">

    <div class="form-group col-md-4">
      <label for="adresse">ADRESSE</label>
      <input type="text" class="form-control" id="adresse" placeholder="ADRESSE">
    </div>
    <div class="form-group col-md-4">
   	<label for="numtel">NUMERO TELEPHONE</label>
      <input type="text" class="form-control" id="numtel" placeholder="NUMERO TELEPHONE">
    </div>
    <div class="form-group col-md-4">
      <label for="fkt">FOKONTANY</label>
      <input type="text" class="form-control" id="fkt" placeholder="FOKONTANY">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="commune">COMMUNE</label>
      <input type="text" class="form-control" id="commune" placeholder="COMMUNE">
    </div>
    <div class="form-group col-md-4">
   	<label for="district">DISTRICT</label>
      <input type="text" class="form-control" id="district" placeholder="DISTRICT">
    </div>
    <div class="form-group col-md-4">
      <label for="region">REGION</label>
      <input type="text" class="form-control" id="region" placeholder="REGION">
    </div>
  </div>

  <div class = "panel panel-primary">
   <div class = "panel-heading">
      <h3 class = "panel-title">FISCAL / BANCAIRE</h3>
   </div>
  </div>


    <div class="form-row">
    <div class="form-group col-md-4">
      <label for="NIF">NIF</label>
      <input type="text" class="form-control" id="nif" placeholder="nif">
    </div>
    <div class="form-group col-md-4">
   	<label for="stat">STAT </label>
      <input type="text" class="form-control" id="stat" placeholder="STAT">
    </div>
    <div class="form-group col-md-4">
      <label for="rai_soc">RAISON SOCIAL</label>
      <input type="text" class="form-control" id="rai_soc" placeholder="RAISON SOCIAL">
    </div>
  </div>

    <div class="form-row">
    <div class="form-group col-md-4">
      <label for="contact">CONTACT</label>
      <input type="text" class="form-control" id="contact" placeholder="CONTACT">
    </div>
    <div class="form-group col-md-4">
   	<label for="num_cb">NUMERO COMPTE BANCAIRE</label>
      <input type="text" class="form-control" id="num_cb" placeholder="NUMERO COMPTE BANCAIRE">
    </div>
    <div class="form-group col-md-4">
      <label for="banque">BANQUE</label>
      <input type="text" class="form-control" id="banque" placeholder="RAISON SOCIAL">
    </div>
  </div>


    <div class="form-row">
    <div class="form-group col-md-6">
      <label for="adresse_bank">ADRESSE BANCAIRE</label>
      <input type="text" class="form-control" id="adresse_bank" placeholder="ADRESSE BANCAIRE">
    </div>
  </div>

  <button type="submit" class="btn btn-primary btn-large">ENREGISTRER</button>

 <div class="form-row">
    <div class="form-group col-md-6">
    </div>
  </div>


</form>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright ? Your Website 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">?</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<c:url value='/static/vendor/jquery/jquery.min.js'/>"></script>
    <script src="<c:url value='/static/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<c:url value='/static/vendor/jquery-easing/jquery.easing.min.js'/>"></script>

    <!-- Page level plugin JavaScript-->
    <script src="<c:url value='/static/vendor/datatables/jquery.dataTables.js'/>"></script>
    <script src="<c:url value='/static/vendor/datatables/dataTables.bootstrap4.js'/>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<c:url value='/static/js/sb-admin.min.js'/>"></script>

    <!-- Demo scripts for this page-->
    <script src="<c:url value='/static/js/demo/datatables-demo.js'/>"></script>

  </body>

</html>
