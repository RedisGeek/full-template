<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>User Registration Form</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>

 	<div class="generic-container">
	<div class="well lead">Inscription operateur</div>
 	<form:form method="POST" modelAttribute="operateur" class="form-horizontal">
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="fokotany">Fokotany</label>
				<div class="col-md-7">
					<form:select path="fokotany" items="${fokontany}" multiple="false" itemValue="clefkt" itemLabel="nomfkt" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="fokotany" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="banque">Banque</label>
				<div class="col-md-7">
					<form:select path="banque" items="${banques}" multiple="false" itemValue="clebanque" itemLabel="nombanque" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="banque" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="codetyp">Type d'operateur</label>
                <div class="col-md-7">
                    <form:select path="codetyp" items="${types}" multiple="false" itemValue="codetyp" itemLabel="designationType" class="form-control input-sm" />
                    <div class="has-error">
                        <form:errors path="codetyp" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="nomexploitant">nom</label>
				<div class="col-md-7">
					<form:input type="text" path="nomexploitant" id="nomexploitant" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="nomexploitant" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="prenomexpl">prenom</label>
				<div class="col-md-7">
					<form:input type="text" path="prenomexpl" id="prenomexpl" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="prenomexpl" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="datenaiss">date de naissance</label>
				<div class="col-md-7">
					<form:input type="date" path="datenaiss" id="datenaiss" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="datenaiss" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="sexe">Sexe</label>
				<div class="col-md-7">
					<form:input type="text" path="sexe" id="sexe" class="form-control input-sm"/>
					<div class="has-error">
						<form:errors path="sexe" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="age">Age</label>
				<div class="col-md-7">
					<form:input type="text" path="age" id="age" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="nomexploitant" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="adresse">Adresse</label>
				<div class="col-md-7">
					<form:input type="text" path="adresse" id="adresse" class="form-control input-sm"/>
					<div class="has-error">
						<form:errors path="adresse" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="numcin">Num CIN</label>
				<div class="col-md-7">
					<form:input type="text" path="numcin" id="numcin" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="numcin" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="lieucin">lieucin</label>
				<div class="col-md-7">
					<form:input type="text" path="lieucin" id="lieucin" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="lieucin" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="datecin">Date CIN</label>
				<div class="col-md-7">
					<form:input type="date" path="datecin" id="datecin" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="datecin" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="numtel">Num tel</label>
				<div class="col-md-7">
					<form:input type="text" path="numtel" id="numtel" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="numtel" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="nif">nif</label>
				<div class="col-md-7">
					<form:input type="text" path="nif" id="nif" class="form-control input-sm"/>
					<div class="has-error">
						<form:errors path="nif" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="stat">stat</label>
				<div class="col-md-7">
					<form:input type="text" path="stat" id="stat" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="stat" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-3 control-lable" for="raisonsocial">raison social</label>
				<div class="col-md-7">
					<form:input type="text" path="raisonsocial" id="raisonsocial" class="form-control input-sm" />
					<div class="has-error">
						<form:errors path="raisonsocial" class="help-inline"/>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-actions floatRight">
				<c:choose>
					<c:when test="${edit}">
						<input type="submit" value="Update" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/list' />">Cancel</a>
					</c:when>
					<c:otherwise>
						<input type="submit" value="Register" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/list' />">Cancel</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		
		<c:if test="${edit}">
			<span class="well pull-left">
				<a href="<c:url value='/add-document-${user.id}' />">Click here to upload/manage your documents</a>	
			</span>
		</c:if>
		
	</form:form>
	</div>
</body>
</html>