<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Users List</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<div class="generic-container">
		<div class="panel panel-default">
			  <!-- Default panel contents -->
		  	<div class="panel-heading"><span class="lead">Liste des operateur </span></div>
		  	<div class="tablecontainer">
				<table class="table table-hover">
		    		<thead>
			      		<tr>
					        <th>Nom</th>
					        <th>Prenom</th>
					        <th>Sexe</th>
					        <th>CIN</th>
					        <th width="100"></th>
					        <th width="100"></th>
						</tr>
			    	</thead>
		    		<tbody>
					<c:forEach items="${operations}" var="operations">

						<tr>
							<td>${operations.nomexploitant}</td>
							<td>${operations.prenomexpl}</td>
							<c:if test="${operations.sexe==1}">
								<td>Masculin</td>
						</c:if>
							<c:if test="${operations.sexe==2}">
								<td>Feminin</td>
							</c:if>

							<td>${operations.numcin}</td>
							<td><a href="<c:url value='/edit-user-${operations.codeexpl}' />" class="btn btn-success custom-width">edit</a></td>
							<td><a href="<c:url value='/delete-user-${operations.codeexpl}' />" class="btn btn-danger custom-width">delete</a></td>
						</tr>
					</c:forEach>
		    		</tbody>
		    	</table>
		    </div>
		</div>
		<div class="well">
			<a href="<c:url value='/EnregistreOperateur' />">Ajouter operateur</a>
		</div>
   	</div>
</body>
</html>