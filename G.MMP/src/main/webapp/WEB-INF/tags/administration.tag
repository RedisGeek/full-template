<%@tag description="template sigrjb" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
   <head>
       <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="description" content="helios">
       <meta name="keywords" content="HTML,CSS,XML,JavaScript">
       <meta name="author" content="helios">
       <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <title>MMP - OMDATA</title>

       <!-- CSS -->
       <!-- bootstrap -->
       <link rel="stylesheet" href="static/css/bootstrap/bootstrap.min.css" type="text/css" />
       <link rel="stylesheet" href="static/css/bootstrap.css" type="text/css" />
       <link rel="stylesheet" href="static/css/fontawesome/all.min.css" type="text/css" />
       <link rel="stylesheet" href="static/css/sb-admin.css" />

       <style type="text/css">
           /* Chart.js */
           @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}
       </style>

       <!-- JS -->
       <script type="text/javascript" src="static/js/jquery/jquery.min.js"></script>
       <script type="text/javascript" src="static/js/bootstrap/bootstrap.bundle.min.js"></script>
       <script type="text/javascript" src="static/js/jquery/jquery.easing.min.js"></script>

   </head>
   <body id="page-top">
      <nav class="class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="">MMP - OMDATA</a>
        <button id="sidebarToggle" class="btn btn-link btn-sm text-white order-1 order-sm-0" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown no-arrow">
                <a id="userDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user-circle fa-fw"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout </a>
                </div>
            </li>
        </ul>
      </nav>

      <div id="wrapper">
          <ul class="sidebar navbar-nav">
              <li class="nav-item active">
                  <a class="nav-link" href="/index">
                      <i class="fas fa-fw fa-tachometer-alt"></i>
                      <span>Tableau de bord</span>
                  </a>
              </li>
              <li class="nav-item dropdown">
                  <a id="pagesDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-fw fa-folder"></i>
                      <span>Liste exploitants</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="">
                      <i class="fas fa-fw fa-chart-area"></i>
                      <span>Liste colleteur</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="">
                      <i class="fas fa-fw fa-table"></i>
                      <sapn>Gestion des produits</sapn>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="">
                      <i class="fas fa-fw fa-table"></i>
                      <sapn>Gestion des utilisateurs</sapn>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="">
                      <i class="fas fa-fw fa-table"></i>
                      <sapn>Gestion perception</sapn>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="">
                      <i class="fas fa-fw fa-table"></i>
                      <sapn>Liste des action</sapn>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="">
                      <i class="fas fa-fw fa-table"></i>
                      <sapn>Exportation</sapn>
                  </a>
              </li>
          </ul>
      </div>

      <jsp:doBody />
      <footer>
          <div class="container my-auto">
              <div class="copyright text-center my-auto">
                  <span>Copyright © OMDATA 2018</span>
              </div>
          </div>
      </footer>
      <a class="scroll-to-top rounded" href="#page-top" style="display: inline;">
          <i class="fas fa-angle-up"></i>
      </a>
   </body>

</html>

