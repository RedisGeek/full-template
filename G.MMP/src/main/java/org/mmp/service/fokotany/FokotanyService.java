package org.mmp.service.fokotany;

import org.mmp.model.Fokontany;

import java.util.List;

public interface FokotanyService {

    List<Fokontany> findAll();

    Fokontany findByNomFkt(String nameFkt);

    Fokontany findById(int id);
}
