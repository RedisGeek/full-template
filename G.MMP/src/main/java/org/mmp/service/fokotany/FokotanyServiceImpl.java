package org.mmp.service.fokotany;

import org.mmp.dao.fonkotany.FokotanyDao;
import org.mmp.model.Fokontany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class FokotanyServiceImpl implements FokotanyService {
    @Autowired
    private FokotanyDao fokotanyDao;

    public List<Fokontany> findAll() {
        return fokotanyDao.findAll();
    }

    public Fokontany findByNomFkt(String nameFkt) {
        return fokotanyDao.findByNomFkt(nameFkt);
    }

    public Fokontany findById(int id) {
        System.out.println(id);
        return fokotanyDao.findById(id);
    }
}
