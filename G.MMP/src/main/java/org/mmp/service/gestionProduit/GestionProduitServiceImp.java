package org.mmp.service.gestionProduit;

import org.mmp.dao.gestionProduit.ProduitDao;
import org.mmp.model.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GestionProduitServiceImp implements GestionProduitService{

    @Autowired
    private ProduitDao produitDao;

    public List<Produit> findAll() {
        List<Produit> produits = produitDao.findAll();

        return produits;
    }
    public Produit findById(int id) {
        Produit produit = produitDao.findById(id);
        return produit;
    }
}
