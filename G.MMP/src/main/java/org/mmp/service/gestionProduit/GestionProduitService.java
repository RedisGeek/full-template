package org.mmp.service.gestionProduit;

import org.mmp.model.Produit;

import java.util.List;

public interface GestionProduitService {

    List<Produit> findAll();

    Produit findById(int id);
}
