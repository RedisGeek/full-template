package org.mmp.service.enquete;

import org.mmp.dao.enquete.EnqueteDao;
import org.mmp.dao.operator.OperateurDao;
import org.mmp.model.EnqueteExploitation;
import org.mmp.model.Operateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("enqueteoperateurService")
@Transactional
public class EnqueteServiceImpl implements EnqueteService {

	@Autowired
	private EnqueteDao dao;

	public EnqueteExploitation findById(int id) {
		return dao.findById(id);
	}

    public List<EnqueteExploitation> findAllPercepteur() {
       return dao.findAll();
    }

    public EnqueteExploitation save(EnqueteExploitation enqueteExploitation) {
		dao.save(enqueteExploitation);
		return enqueteExploitation;
	}

    public List<EnqueteExploitation> findByPercepteur(Operateur percepteur) {
        return dao.findByPercepteur(percepteur);
    }
	
}
