package org.mmp.service.enquete;

import org.mmp.model.EnqueteExploitation;
import org.mmp.model.Operateur;

import java.util.List;


public interface EnqueteService {
	
	EnqueteExploitation findById(int id);
	List<EnqueteExploitation> findAllPercepteur();
	EnqueteExploitation save(EnqueteExploitation enqueteExploitation);
    List<EnqueteExploitation> findByPercepteur(Operateur percepteur);

}