package org.mmp.service.commune;

import org.mmp.model.Commune;

import java.util.List;

public interface CommuneService {

    List<Commune> findAll();

    Commune findByNomComm(String nomComm);

    Commune findById(int id);
}
