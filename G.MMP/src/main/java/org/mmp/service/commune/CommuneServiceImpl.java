package org.mmp.service.commune;

import org.mmp.dao.commun.CommuneDao;
import org.mmp.model.Commune;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CommuneServiceImpl implements CommuneService{

    @Autowired
    private CommuneDao communeDao;

    public List<Commune> findAll() {
        return communeDao.findAll();
    }

    public Commune findByNomComm(String nomComm) {
        return communeDao.findByNomComm(nomComm);
    }


    public Commune findById(int id) {
        return null;
    }
}
