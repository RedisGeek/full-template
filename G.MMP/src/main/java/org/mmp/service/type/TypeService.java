package org.mmp.service.type;

import org.mmp.model.Fokontany;
import org.mmp.model.Type;

import java.util.List;

public interface TypeService {

    List<Type> findAll();

    Type findDesignatioType(String designatioType);

    Type findById(int id);
}
