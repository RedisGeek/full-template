package org.mmp.service.type;

import org.mmp.dao.type.TypeDao;
import org.mmp.model.Fokontany;
import org.mmp.model.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("typeService")
@Transactional
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeDao typeDao;


    public List<Type> findAll() {
        return typeDao.findAll();
    }

    public Type findDesignatioType(String designatioType) {
        return typeDao.findDesignatioType(designatioType);
    }

    public Type findById(int id) {
        return typeDao.findById(id);
    }
}
