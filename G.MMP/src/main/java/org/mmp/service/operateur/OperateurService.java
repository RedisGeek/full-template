package org.mmp.service.operateur;

import org.mmp.model.Operateur;

import java.util.List;


public interface OperateurService {
	
	Operateur findById(int id);
	List<Operateur> findAll();
	Operateur save(Operateur user);

}