package org.mmp.service.operateur;

import org.mmp.dao.operator.OperateurDao;
import org.mmp.model.Operateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("operateurService")
@Transactional
public class OperateurServiceImpl implements OperateurService {

	@Autowired
	private OperateurDao dao;

	public Operateur findById(int id) {
		return dao.findById(id);
	}

	public Operateur save(Operateur operateur) {
		dao.save(operateur);
		return operateur;
	}
	public List<Operateur> findAll() {
		return dao.findAll();
	}
	
}
