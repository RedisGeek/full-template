package org.mmp.service.bank;

import org.mmp.model.Banque;

import java.util.List;

public interface BankService {

    Banque findByClebanque(int id);

    void save(Banque banque);
    List<Banque> findAllBanque();
}
