package org.mmp.service.bank;

import org.mmp.dao.bank.BankDao;
import org.mmp.model.Banque;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("bankService")
@Transactional
public class BankServiceImpl implements BankService {
    @Autowired
    private BankDao dao;

    public Banque findByClebanque(int id) {
        return dao.findByClebanque(id);
    }

    public void save(Banque banque) {
        dao.save(banque);
    }

    public List<Banque> findAllBanque() {
        return dao.findAll();
    }
}
