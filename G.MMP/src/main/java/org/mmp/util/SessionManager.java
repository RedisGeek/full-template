package org.mmp.util;



import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.mmp.model.Operateur;
import org.mmp.model.Operateurs;
import java.util.*;


public class SessionManager {
    private static SessionManager ourInstance = null;

    public static SessionManager getInstance() {
        if (ourInstance == null){

            ourInstance = new SessionManager();
        }
        return ourInstance;

    }
    private SessionManager() {
    }
    public SessionFactory sessionFactory() {
       SessionFactory sessionFactory=null;
       Object sessionFactoryMonitor = new Object();
        if(sessionFactory==null || sessionFactory.isClosed()){
            synchronized (sessionFactoryMonitor) {
                if (sessionFactory == null || sessionFactory.isClosed()) {
                    try {
                        sessionFactory = new Configuration().configure()
                                .addAnnotatedClass(Operateur.class).buildSessionFactory();
                    } catch (Throwable ex) {
                        ex.fillInStackTrace();
                    }
                }
            }
        }

        return sessionFactory;
    }
    public List<Operateur> searchMultiCritial(Operateur operateur){
        Transaction transaction=null;
        try  {
            Session newSession = sessionFactory().openSession();
            transaction = newSession.beginTransaction();
            transaction.commit();

            List<Operateur> operateurs = new ArrayList<Operateur>();
            String requete = "select * from operateur where 1=1";
            Query query = null;
            if(operateur.getAdresse()!="")
            {
                requete+=" AND operateur.ADRESSE =:adresse";
                query.setParameter("adresse",operateur.getAdresse());
            }
            if(operateur.getNomexploitant()!="")
            {
                requete+=" AND operateur.nomexploitant like :exploit";
                query.setParameter("exploit",'%'+operateur.getNomexploitant()+'%');
            }
            if(operateur.getNumcin()!="")
            {
                requete+=" AND operateur.numcin =:cin";
                query.setParameter("cin",operateur.getNumcin());
            }
            if(operateur.getNif()!="")
            {
                requete+=" AND operateur.nif = :nif";
                query.setParameter("nif",operateur.getNif());
            }
            if(operateur.getStat()!="")
            {
                requete+=" AND operateur.stat = :stat";
                query.setParameter("stat",operateur.getStat());
            }
            if(operateur.getCodeexpl()!=null)
            {
                requete+=" AND operateur.codeexpl=:exploitant";
                query.setParameter("exploitant",operateur.getCodeexpl());
            }
            if(operateur.getRaisonsocial()!="")
            {
                requete+=" AND p.raisonsocial like :raisonsocial";
                query.setParameter("raisonsocial",'%'+operateur.getRaisonsocial()+'%');
            }
            if(operateur.getFokotany()!=null)
            {
                requete+=" AND operateur.fokotany.clefkt = :clefkt";
                query.setParameter("clefkt",'%'+operateur.getFokotany().getClefkt()+'%');
            }
            query = newSession.createQuery(requete);
            System.out.println(query);
            operateurs.addAll(query.list());

            System.out.println("--------"+operateurs.size()+"-------------");
            return operateurs;
        } catch (Throwable ex) {
            return Collections.EMPTY_LIST;
        }



    }

}
