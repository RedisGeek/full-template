package org.mmp.controller;

import org.mmp.model.Produit;
import org.mmp.service.gestionProduit.GestionProduitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class GestionProduitController {

    @Autowired
    GestionProduitService gestionProduitService;


    @RequestMapping(value = { "/list_produit" }, method = RequestMethod.GET)
    public String listProduits(ModelMap model) {

        List<Produit> produits = gestionProduitService.findAll();
        model.addAttribute("produits", produits);
        return "produitList";
    }
    @RequestMapping(value = { "/enregistreProduit" }, method = RequestMethod.GET)
    public String saveProduit(ModelMap model) {
        Produit pr = new Produit();
        model.addAttribute("produit", pr);
        model.addAttribute("edit", false);
        return "registration";
    }

}
