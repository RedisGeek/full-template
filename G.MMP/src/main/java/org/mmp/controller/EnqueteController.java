package org.mmp.controller;

import org.mmp.model.EnqueteExploitation;
import org.mmp.model.Operateur;
import org.mmp.service.enquete.EnqueteService;
import org.mmp.service.operateur.OperateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

public class EnqueteController {
    @Controller
    @SessionAttributes("operateurs")
    public class GestionOperationController {
        @Autowired
        OperateurService operateurService;

        @Autowired
        EnqueteService enqueteService;

        @RequestMapping(value = {"/list_enquete"}, method = RequestMethod.GET)
        public String listOperations(ModelMap model) {

            List<EnqueteExploitation> operateurs = enqueteService.findAllPercepteur();
            model.addAttribute("enquetes", operateurs);
            return "enquetes";
        }

        @RequestMapping(value = {"/EnregistreEnquete"}, method = RequestMethod.GET)
        public String saveEnquete(ModelMap model) {
            EnqueteExploitation op = new EnqueteExploitation();
            model.addAttribute("enquete", op);
            model.addAttribute("edit", false);

            return "enquete";
        }

        @RequestMapping(value = {"/EnregistreOperateur"}, method = RequestMethod.POST)
        public String saveUser(@Valid EnqueteExploitation enqueteExploitation, BindingResult result,
                               ModelMap model) {

            Operateur operateur = operateurService.findById(Integer.parseInt(result.getFieldValue("operateur").toString()));
            //enqueteExploitation.setCodeexpl(operateur);
            enqueteService.save(enqueteExploitation);
            List<EnqueteExploitation> enquets = enqueteService.findAllPercepteur();
            model.addAttribute("enquetes", enquets);
            model.addAttribute("success", "Operateur " + enqueteExploitation.getCodequest() + " registered successfully");
            return "enqueteList";
        }

        @ModelAttribute("operateurs")
        public List<Operateur> initializeOperateur() {
            return operateurService.findAll();
        }
    }
}
