package org.mmp.controller;

import org.mmp.model.*;
import org.mmp.service.bank.BankService;
import org.mmp.service.fokotany.FokotanyService;
import org.mmp.service.operateur.OperateurService;
import org.mmp.service.type.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@SessionAttributes("fokontany")
public class GestionOperationController {
    @Autowired
    OperateurService operateurService;

    @Autowired
    FokotanyService fokotanyService;

    @Autowired
    BankService banqueService;
    @Autowired
    TypeService typeService;

    @InitBinder
    public void setPropertyBinder(WebDataBinder dataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        dataBinder.registerCustomEditor(Date.class, editor);
    }
    @RequestMapping(value = { "/list_operation" }, method = RequestMethod.GET)
    public String listOperations(ModelMap model) {

        List<Operateur> operateurs = operateurService.findAll();
        model.addAttribute("operations", operateurs);
        return "operateurList";
    }
    @RequestMapping(value = { "/EnregistreOperateur" }, method = RequestMethod.GET)
    public String saveOperateur(ModelMap model) {
        Operateur op = new Operateur();
        model.addAttribute("operateur", op);
        model.addAttribute("edit", false);

        return "registrationOperation";
    }
    @RequestMapping(value = { "/EnregistreOperateur" }, method = RequestMethod.POST)
    public String saveUser(@Valid Operateur operateur, BindingResult result,
                           ModelMap model) {

        Fokontany fokontany=fokotanyService.findById(Integer.parseInt(result.getFieldValue("fokotany").toString()));
        operateur.setFokotany(fokontany);
        Banque banque=banqueService.findByClebanque(Integer.parseInt(result.getFieldValue("banque").toString()));
        Type type = typeService.findById(Integer.parseInt(result.getFieldValue("codetyp").toString()));
        operateur.setBanque(banque);
        operateur.setCodetyp(type);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        operateurService.save(operateur);
        List<Operateur> ops = operateurService.findAll();
        model.addAttribute("operateurs", ops);
        model.addAttribute("success", "Operateur " + operateur.getRaisonsocial() + " registered successfully");
        return "operateurList";
    }
    @ModelAttribute("fokontany")
    public List<Fokontany> initializeFokotany(){
        return fokotanyService.findAll();
    }
    @ModelAttribute("banques")
    public List<Banque> initializeCommune(){
        return banqueService.findAllBanque();
    }
    @ModelAttribute("types")
    public List<Type> initializeType(){
        return typeService.findAll();
    }
}
