package org.mmp.dao.bank;

import org.hibernate.Criteria;
import org.mmp.dao.AbstractDao;
import org.mmp.model.Banque;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("bankDao")
public class BankDaoImp extends AbstractDao<Integer, Banque> implements BankDao {

    public Banque findByClebanque(int id) {
        Banque banque = getByKey(id);
        return banque;
    }

    public void save(Banque banque) {

        persist(banque);
    }
    public List<Banque> findAll(){
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Banque> banques = criteria.list();
        return banques;
    }
}
