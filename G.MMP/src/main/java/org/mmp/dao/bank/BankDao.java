package org.mmp.dao.bank;

import org.mmp.model.Banque;

import java.util.List;

public interface BankDao {
    Banque findByClebanque(int id);

    void save(Banque banque);
    List<Banque> findAll();



}
