package org.mmp.dao.enquete;

import org.mmp.model.EnqueteExploitation;
import org.mmp.model.Fokontany;
import org.mmp.model.Operateur;

import java.util.List;

public interface EnqueteDao {

    List<EnqueteExploitation> findAll();

    List<EnqueteExploitation> findByPercepteur(Operateur percepteur);

    EnqueteExploitation save(EnqueteExploitation enquete);

    EnqueteExploitation findById(int id);
}
