package org.mmp.dao.enquete;


import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.mmp.dao.AbstractDao;
import org.mmp.model.EnqueteExploitation;
import org.mmp.model.Fokontany;
import org.mmp.model.Operateur;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("enqueteDao")
public class EnqueteDaoImp extends AbstractDao<Integer, EnqueteExploitation> implements EnqueteDao {

    public List<EnqueteExploitation> findAll() {
        Criteria crit = createEntityCriteria();
        return (List<EnqueteExploitation>) crit.list();
    }

    public List<EnqueteExploitation> findByPercepteur(Operateur percepteur) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("CODEEXPL",percepteur));
        return criteria.list();
    }

    public EnqueteExploitation findById(int id) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("CODEREQUEST",id));
        EnqueteExploitation enqueteExploitation = (EnqueteExploitation) criteria.uniqueResult();
        return enqueteExploitation;
    }
    public  EnqueteExploitation save(EnqueteExploitation enqueteExploitation) {
        persist(enqueteExploitation);
        return  enqueteExploitation;
    }
}
