package org.mmp.dao.operator;

import com.oracle.javafx.jmx.json.JSONDocument;
import com.oracle.javafx.jmx.json.JSONWriter;
import org.hibernate.Criteria;
import org.mmp.dao.AbstractDao;
import org.mmp.model.Operateur;
import org.mmp.model.Produit;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.Logger;

@Repository("operateurDao")
public class OperateurDaoImp  extends AbstractDao<Integer, Operateur> implements OperateurDao {


    public List<Operateur> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Operateur> operateurs = criteria.list();
        return operateurs;
    }
    public Operateur findById(int id) {
        Criteria crit = createEntityCriteria();
        Operateur operateur = (Operateur) crit.uniqueResult();
        return operateur;
    }
    public  int save(Operateur operateur) {
        System.out.println(operateur.getCodeexpl());
        persist(operateur);
        return  operateur.getCodeexpl();
    }
}
