package org.mmp.dao.operator;

import org.mmp.model.Operateur;
import org.mmp.model.Produit;

import java.util.List;

public interface OperateurDao {

    List<Operateur> findAll();

    Operateur findById(int id);

    int save(Operateur operateur);
}
