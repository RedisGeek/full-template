package org.mmp.dao.commun;

import org.mmp.model.Commune;

import java.util.List;

public interface CommuneDao {

    List<Commune> findAll();

    Commune findByNomComm(String nomComm);

    Commune findById(int id);
}
