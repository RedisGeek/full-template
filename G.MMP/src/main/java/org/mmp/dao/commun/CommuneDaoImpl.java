package org.mmp.dao.commun;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.mmp.dao.AbstractDao;
import org.mmp.model.Commune;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("communDao")
public class CommuneDaoImpl extends AbstractDao<Integer, Commune> implements CommuneDao {

    public List<Commune> findAll() {
        Criteria crit = createEntityCriteria();
        return (List<Commune>) crit.list();
    }

    public Commune findByNomComm(String nomComm) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("NOM_COMM",nomComm));
        Commune commune = (Commune) criteria.uniqueResult();
        return commune;
    }

    public Commune findById(int id) {
        return getByKey(id);
    }
}
