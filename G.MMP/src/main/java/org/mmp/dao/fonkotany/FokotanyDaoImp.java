package org.mmp.dao.fonkotany;


import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.mmp.dao.AbstractDao;
import org.mmp.model.Fokontany;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("fokotanyDao")
public class FokotanyDaoImp extends AbstractDao<Integer, Fokontany> implements FokotanyDao {

    public List<Fokontany> findAll() {
        Criteria crit = createEntityCriteria();
        return (List<Fokontany>) crit.list();
    }

    public Fokontany findByNomFkt(String nameFkt) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("NOMFKT",nameFkt));
        Fokontany fokontany = (Fokontany) criteria.uniqueResult();
        return fokontany;
    }

    public Fokontany findById(int id) {
        return getByKey(id);
    }
}
