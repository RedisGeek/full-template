package org.mmp.dao.fonkotany;

import org.mmp.model.Fokontany;

import java.util.List;

public interface FokotanyDao {

    List<Fokontany> findAll();

    Fokontany findByNomFkt(String nameFkt);

    Fokontany findById(int id);
}
