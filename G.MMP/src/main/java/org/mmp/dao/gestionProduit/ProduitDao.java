package org.mmp.dao.gestionProduit;

import org.mmp.model.Operateur;
import org.mmp.model.Produit;

import java.util.List;

public interface ProduitDao {

    List<Produit> findAll();

    Produit findById(int id);
}
