package org.mmp.dao.gestionProduit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.mmp.dao.AbstractDao;
import org.mmp.model.Produit;
import org.mmp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("produitDao")
public class ProduitDaoImp  extends AbstractDao<Long, Produit> implements ProduitDao {


    public List<Produit> findAll() {
        Criteria criteria = createEntityCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Produit> produits = criteria.list();
        return produits;
    }
    public Produit findById(int id) {
        Criteria crit = createEntityCriteria();
        Produit produit = (Produit) crit.uniqueResult();
        return produit;
    }
}
