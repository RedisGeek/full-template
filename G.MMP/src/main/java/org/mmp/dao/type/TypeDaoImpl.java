package org.mmp.dao.type;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.mmp.dao.AbstractDao;
import org.mmp.model.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("typeDao")
public class TypeDaoImpl extends AbstractDao<Integer, Type> implements TypeDao {

    public List<Type> findAll() {
        Criteria criteria = createEntityCriteria();
        List<Type> typeList = criteria.list();
        return typeList;
    }
    public Type findDesignatioType(String designatioType) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("designatioType",designatioType));
        Type type = (Type) criteria.uniqueResult();
        return type;
    }
    public Type findById(int id) {
        return getByKey(id);
    }
}
