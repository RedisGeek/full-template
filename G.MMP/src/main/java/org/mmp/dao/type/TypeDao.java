package org.mmp.dao.type;

import org.mmp.model.Commune;
import org.mmp.model.Type;

import java.util.List;

public interface TypeDao {
    List<Type> findAll();

    Type findDesignatioType(String designatioType);

    Type findById(int id);
}
