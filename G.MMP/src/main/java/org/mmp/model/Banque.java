/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "banque")
public class Banque {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLEBANQUE")
    private Integer clebanque;
    @Size(max = 20)
    @Column(name = "NOMBANQUE")
    private String nombanque;
    @Size(max = 250)
    @Column(name = "ADRESSEBANQUE")
    private String adressebanque;

    public Banque() {
    }

    public Banque(Integer clebanque) {
        this.clebanque = clebanque;
    }

    public Integer getClebanque() {
        return clebanque;
    }

    public void setClebanque(Integer clebanque) {
        this.clebanque = clebanque;
    }

    public String getNombanque() {
        return nombanque;
    }

    public void setNombanque(String nombanque) {
        this.nombanque = nombanque;
    }

    public String getAdressebanque() {
        return adressebanque;
    }

    public void setAdressebanque(String adressebanque) {
        this.adressebanque = adressebanque;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clebanque != null ? clebanque.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banque)) {
            return false;
        }
        Banque other = (Banque) object;
        if ((this.clebanque == null && other.clebanque != null) || (this.clebanque != null && !this.clebanque.equals(other.clebanque))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Banque[ clebanque=" + clebanque + " ]";
    }
    
}
