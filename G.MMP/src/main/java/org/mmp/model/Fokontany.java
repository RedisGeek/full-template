/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "fokontany")
public class Fokontany {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLEFKT")
    private Integer clefkt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODEFKT")
    private String codefkt;
    @Size(max = 128)
    @Column(name = "NOMFKT")
    private String nomfkt;
    @Column(name = "CODECOM")
    private Integer codecom;

    public Fokontany() {
    }

    public Fokontany(Integer clefkt) {
        this.clefkt = clefkt;
    }

    public Fokontany(Integer clefkt, String codefkt) {
        this.clefkt = clefkt;
        this.codefkt = codefkt;
    }

    public Integer getClefkt() {
        return clefkt;
    }

    public void setClefkt(Integer clefkt) {
        this.clefkt = clefkt;
    }

    public String getCodefkt() {
        return codefkt;
    }

    public void setCodefkt(String codefkt) {
        this.codefkt = codefkt;
    }

    public String getNomfkt() {
        return nomfkt;
    }

    public void setNomfkt(String nomfkt) {
        this.nomfkt = nomfkt;
    }

    public Integer getCodecom() {
        return codecom;
    }

    public void setCodecom(Integer codecom) {
        this.codecom = codecom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clefkt != null ? clefkt.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fokontany)) {
            return false;
        }
        Fokontany other = (Fokontany) object;
        if ((this.clefkt == null && other.clefkt != null) || (this.clefkt != null && !this.clefkt.equals(other.clefkt))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Fokontany[ clefkt=" + clefkt + " ]";
    }
    
}
