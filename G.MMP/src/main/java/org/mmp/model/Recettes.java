/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "recettes")
public class Recettes  {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODRECET")
    private String codrecet;
    @Size(max = 128)
    @Column(name = "DESIGNATIONRECT")
    private String designationrect;
    @Column(name = "TAUX")
    private Integer taux;
    @Column(name = "CODEEXPL")
    private Short codeexpl;
    @Column(name = "CODIMP")
    private Short codimp;

    public Recettes() {
    }

    public Recettes(String codrecet) {
        this.codrecet = codrecet;
    }

    public String getCodrecet() {
        return codrecet;
    }

    public void setCodrecet(String codrecet) {
        this.codrecet = codrecet;
    }

    public String getDesignationrect() {
        return designationrect;
    }

    public void setDesignationrect(String designationrect) {
        this.designationrect = designationrect;
    }

    public Integer getTaux() {
        return taux;
    }

    public void setTaux(Integer taux) {
        this.taux = taux;
    }

    public Short getCodeexpl() {
        return codeexpl;
    }

    public void setCodeexpl(Short codeexpl) {
        this.codeexpl = codeexpl;
    }

    public Short getCodimp() {
        return codimp;
    }

    public void setCodimp(Short codimp) {
        this.codimp = codimp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codrecet != null ? codrecet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recettes)) {
            return false;
        }
        Recettes other = (Recettes) object;
        if ((this.codrecet == null && other.codrecet != null) || (this.codrecet != null && !this.codrecet.equals(other.codrecet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Recettes[ codrecet=" + codrecet + " ]";
    }
    
}
