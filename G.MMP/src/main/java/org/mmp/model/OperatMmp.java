/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "operat_mmp")
public class OperatMmp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "code_expl")
    private Integer codeExpl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "codetyp")
    private int codetyp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nom_expl")
    private String nomExpl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "prenom")
    private String prenom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_naiss")
    @Temporal(TemporalType.DATE)
    private Date dateNaiss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "sexe")
    private String sexe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "age")
    private int age;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "adress")
    private String adress;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numcin")
    private int numcin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datecin")
    @Temporal(TemporalType.DATE)
    private Date datecin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "lieucin")
    private String lieucin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "commune")
    private String commune;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nif")
    private int nif;

    public OperatMmp() {
    }

    public OperatMmp(Integer codeExpl) {
        this.codeExpl = codeExpl;
    }

    public OperatMmp(Integer codeExpl, int codetyp, String nomExpl, String prenom, Date dateNaiss, String sexe, int age, String adress, int numcin, Date datecin, String lieucin, String commune, int nif) {
        this.codeExpl = codeExpl;
        this.codetyp = codetyp;
        this.nomExpl = nomExpl;
        this.prenom = prenom;
        this.dateNaiss = dateNaiss;
        this.sexe = sexe;
        this.age = age;
        this.adress = adress;
        this.numcin = numcin;
        this.datecin = datecin;
        this.lieucin = lieucin;
        this.commune = commune;
        this.nif = nif;
    }

    public Integer getCodeExpl() {
        return codeExpl;
    }

    public void setCodeExpl(Integer codeExpl) {
        this.codeExpl = codeExpl;
    }

    public int getCodetyp() {
        return codetyp;
    }

    public void setCodetyp(int codetyp) {
        this.codetyp = codetyp;
    }

    public String getNomExpl() {
        return nomExpl;
    }

    public void setNomExpl(String nomExpl) {
        this.nomExpl = nomExpl;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getNumcin() {
        return numcin;
    }

    public void setNumcin(int numcin) {
        this.numcin = numcin;
    }

    public Date getDatecin() {
        return datecin;
    }

    public void setDatecin(Date datecin) {
        this.datecin = datecin;
    }

    public String getLieucin() {
        return lieucin;
    }

    public void setLieucin(String lieucin) {
        this.lieucin = lieucin;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public int getNif() {
        return nif;
    }

    public void setNif(int nif) {
        this.nif = nif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeExpl != null ? codeExpl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OperatMmp)) {
            return false;
        }
        OperatMmp other = (OperatMmp) object;
        if ((this.codeExpl == null && other.codeExpl != null) || (this.codeExpl != null && !this.codeExpl.equals(other.codeExpl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.OperatMmp[ codeExpl=" + codeExpl + " ]";
    }
    
}
