/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "district")
public class District {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODEDIST")
    private Short codedist;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODEREG")
    private short codereg;
    @Size(max = 50)
    @Column(name = "NOM_DIST")
    private String nomDist;
    @JoinColumn(name = "CLEREG", referencedColumnName = "CLEREG")
    @ManyToOne(optional = false)
    private Region clereg;

    public District() {
    }

    public District(Short codedist) {
        this.codedist = codedist;
    }

    public District(Short codedist, short codereg) {
        this.codedist = codedist;
        this.codereg = codereg;
    }

    public Short getCodedist() {
        return codedist;
    }

    public void setCodedist(Short codedist) {
        this.codedist = codedist;
    }

    public short getCodereg() {
        return codereg;
    }

    public void setCodereg(short codereg) {
        this.codereg = codereg;
    }

    public String getNomDist() {
        return nomDist;
    }

    public void setNomDist(String nomDist) {
        this.nomDist = nomDist;
    }

    public Region getClereg() {
        return clereg;
    }

    public void setClereg(Region clereg) {
        this.clereg = clereg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codedist != null ? codedist.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof District)) {
            return false;
        }
        District other = (District) object;
        if ((this.codedist == null && other.codedist != null) || (this.codedist != null && !this.codedist.equals(other.codedist))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.District[ codedist=" + codedist + " ]";
    }
    
}
