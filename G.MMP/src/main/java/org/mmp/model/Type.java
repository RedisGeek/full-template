/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "type")
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODETYP")
    private Integer codetyp;
    @Size(max = 200)
    @Column(name = "DESIGNATION_TYPE")
    private String designationType;

    public Type() {
    }

    public Type(Integer codetyp) {
        this.codetyp = codetyp;
    }

    public Integer getCodetyp() {
        return codetyp;
    }

    public void setCodetyp(Integer codetyp) {
        this.codetyp = codetyp;
    }

    public String getDesignationType() {
        return designationType;
    }

    public void setDesignationType(String designationType) {
        this.designationType = designationType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codetyp != null ? codetyp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Type)) {
            return false;
        }
        Type other = (Type) object;
        if ((this.codetyp == null && other.codetyp != null) || (this.codetyp != null && !this.codetyp.equals(other.codetyp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Type[ codetyp=" + codetyp + " ]";
    }
    
}
