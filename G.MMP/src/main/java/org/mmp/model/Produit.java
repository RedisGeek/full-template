/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "produit")
public class Produit  {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODPRODT")
    private String codprodt;
    @Size(max = 120)
    @Column(name = "DESIGNATION")
    private String designation;
    @Size(max = 128)
    @Column(name = "DETAILS")
    private String details;
    @Size(max = 128)
    @Column(name = "CLASSE2")
    private String classe2;

    public Produit() {
    }

    public Produit(String codprodt) {
        this.codprodt = codprodt;
    }

    public String getCodprodt() {
        return codprodt;
    }

    public void setCodprodt(String codprodt) {
        this.codprodt = codprodt;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getClasse2() {
        return classe2;
    }

    public void setClasse2(String classe2) {
        this.classe2 = classe2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codprodt != null ? codprodt.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produit)) {
            return false;
        }
        Produit other = (Produit) object;
        if ((this.codprodt == null && other.codprodt != null) || (this.codprodt != null && !this.codprodt.equals(other.codprodt))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Produit[ codprodt=" + codprodt + " ]";
    }
    
}
