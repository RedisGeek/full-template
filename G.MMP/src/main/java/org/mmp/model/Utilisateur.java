/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "utilisateur")
public class Utilisateur {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLEUTIL")
    private Short cleutil;
    @Size(max = 128)
    @Column(name = "CODUTIL")
    private String codutil;
    @Column(name = "CODEGROUP")
    private Short codegroup;
    @Column(name = "CODEAGENT")
    private Short codeagent;
    @Size(max = 128)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 128)
    @Column(name = "PRENOM")
    private String prenom;
    @Size(max = 64)
    @Column(name = "MAIL")
    private String mail;
    @Size(max = 255)
    @Column(name = "TEL")
    private String tel;
    @Column(name = "ID_AGENT")
    private Short idAgent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    @Column(name = "LOGIN")
    private String login;
    @Column(name = "ETAT")
    private Integer etat;

    public Utilisateur() {
    }

    public Utilisateur(Short cleutil) {
        this.cleutil = cleutil;
    }

    public Utilisateur(Short cleutil, String login) {
        this.cleutil = cleutil;
        this.login = login;
    }

    public Short getCleutil() {
        return cleutil;
    }

    public void setCleutil(Short cleutil) {
        this.cleutil = cleutil;
    }

    public String getCodutil() {
        return codutil;
    }

    public void setCodutil(String codutil) {
        this.codutil = codutil;
    }

    public Short getCodegroup() {
        return codegroup;
    }

    public void setCodegroup(Short codegroup) {
        this.codegroup = codegroup;
    }

    public Short getCodeagent() {
        return codeagent;
    }

    public void setCodeagent(Short codeagent) {
        this.codeagent = codeagent;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Short getIdAgent() {
        return idAgent;
    }

    public void setIdAgent(Short idAgent) {
        this.idAgent = idAgent;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cleutil != null ? cleutil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilisateur)) {
            return false;
        }
        Utilisateur other = (Utilisateur) object;
        if ((this.cleutil == null && other.cleutil != null) || (this.cleutil != null && !this.cleutil.equals(other.cleutil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Utilisateur[ cleutil=" + cleutil + " ]";
    }
    
}
