/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "operateur")
public class Operateur {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODEEXPL")
    private Integer codeexpl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "NOMEXPLOITANT")
    private String nomexploitant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "PRENOMEXPL")
    private String prenomexpl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATENAISS")
    @Temporal(TemporalType.DATE)
    private Date datenaiss;
    @Size(max = 2)
    @Column(name = "SEXE")
    private String sexe;
    @Size(max = 10)
    @Column(name = "AGE")
    private String age;
    @Size(max = 128)
    @Column(name = "ADRESSE")
    private String adresse;
    @Size(max = 12)
    @Column(name = "NUMCIN")
    private String numcin;
    @Column(name = "DATECIN")
    @Temporal(TemporalType.DATE)
    private Date datecin;
    @Size(max = 64)
    @Column(name = "LIEUCIN")
    private String lieucin;
    @Size(max = 25)
    @Column(name = "NUMTEL")
    private String numtel;
    @Size(max = 12)
    @Column(name = "NIF")
    private String nif;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALIDE")
    private int valide;
    @Size(max = 20)
    @Column(name = "STAT")
    private String stat;
    @Size(max = 20)
    @Column(name = "RAISONSOCIAL")
    private String raisonsocial;
    @JoinColumn(name = "CODETYP", referencedColumnName = "CODETYP")
    @ManyToOne
    private Type codetyp;
    @JoinColumn(name = "BANQUE", referencedColumnName = "CLEBANQUE")
    @ManyToOne
    private Banque banque;
    @JoinColumn(name = "FOKOTANY", referencedColumnName = "CLEFKT")
    @ManyToOne
    private Fokontany fokotany;

    public Operateur() {
    }

    public Operateur(Integer codeexpl) {
        this.codeexpl = codeexpl;
    }

    public Operateur(Integer codeexpl, String nomexploitant, String prenomexpl, Date datenaiss, int valide) {
        this.codeexpl = codeexpl;
        this.nomexploitant = nomexploitant;
        this.prenomexpl = prenomexpl;
        this.datenaiss = datenaiss;
        this.valide = valide;
    }

    public Integer getCodeexpl() {
        return codeexpl;
    }

    public void setCodeexpl(Integer codeexpl) {
        this.codeexpl = codeexpl;
    }

    public String getNomexploitant() {
        return nomexploitant;
    }

    public void setNomexploitant(String nomexploitant) {
        this.nomexploitant = nomexploitant;
    }

    public String getPrenomexpl() {
        return prenomexpl;
    }

    public void setPrenomexpl(String prenomexpl) {
        this.prenomexpl = prenomexpl;
    }

    public Date getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(Date datenaiss) {
        this.datenaiss = datenaiss;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumcin() {
        return numcin;
    }

    public void setNumcin(String numcin) {
        this.numcin = numcin;
    }

    public Date getDatecin() {
        return datecin;
    }

    public void setDatecin(Date datecin) {
        this.datecin = datecin;
    }

    public String getLieucin() {
        return lieucin;
    }

    public void setLieucin(String lieucin) {
        this.lieucin = lieucin;
    }

    public String getNumtel() {
        return numtel;
    }

    public void setNumtel(String numtel) {
        this.numtel = numtel;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public int getValide() {
        return valide;
    }

    public void setValide(int valide) {
        this.valide = valide;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getRaisonsocial() {
        return raisonsocial;
    }

    public void setRaisonsocial(String raisonsocial) {
        this.raisonsocial = raisonsocial;
    }

    public Type getCodetyp() {
        return codetyp;
    }

    public void setCodetyp(Type codetyp) {
        this.codetyp = codetyp;
    }

    public Banque getBanque() {
        return banque;
    }

    public void setBanque(Banque banque) {
        this.banque = banque;
    }

    public Fokontany getFokotany() {
        return fokotany;
    }

    public void setFokotany(Fokontany fokotany) {
        this.fokotany = fokotany;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeexpl != null ? codeexpl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operateur)) {
            return false;
        }
        Operateur other = (Operateur) object;
        if ((this.codeexpl == null && other.codeexpl != null) || (this.codeexpl != null && !this.codeexpl.equals(other.codeexpl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Operateur[ codeexpl=" + codeexpl + " ]";
    }

}
