/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "commune")
@XmlRootElement
public class Commune {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODECOM")
    private Integer codecom;
    @Size(max = 128)
    @Column(name = "NOM_COMM")
    private String nomComm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODEDIST")
    private short codedist;
    @JoinColumn(name = "CODAGCOM", referencedColumnName = "CODAGCOM")
    @ManyToOne
    private AgentComm codagcom;

    public Commune() {
    }

    public Commune(Integer codecom) {
        this.codecom = codecom;
    }

    public Commune(Integer codecom, short codedist) {
        this.codecom = codecom;
        this.codedist = codedist;
    }

    public Integer getCodecom() {
        return codecom;
    }

    public void setCodecom(Integer codecom) {
        this.codecom = codecom;
    }

    public String getNomComm() {
        return nomComm;
    }

    public void setNomComm(String nomComm) {
        this.nomComm = nomComm;
    }

    public short getCodedist() {
        return codedist;
    }

    public void setCodedist(short codedist) {
        this.codedist = codedist;
    }

    public AgentComm getCodagcom() {
        return codagcom;
    }

    public void setCodagcom(AgentComm codagcom) {
        this.codagcom = codagcom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codecom != null ? codecom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commune)) {
            return false;
        }
        Commune other = (Commune) object;
        if ((this.codecom == null && other.codecom != null) || (this.codecom != null && !this.codecom.equals(other.codecom))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Commune[ codecom=" + codecom + " ]";
    }
    
}
