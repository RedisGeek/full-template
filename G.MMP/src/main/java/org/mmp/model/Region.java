/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "region")
public class Region implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLEREG")
    private Short clereg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODEREG")
    private short codereg;
    @Size(max = 20)
    @Column(name = "NOM_REG")
    private String nomReg;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clereg")
    private List<District> districtList;

    public Region() {
    }

    public Region(Short clereg) {
        this.clereg = clereg;
    }

    public Region(Short clereg, short codereg) {
        this.clereg = clereg;
        this.codereg = codereg;
    }

    public Short getClereg() {
        return clereg;
    }

    public void setClereg(Short clereg) {
        this.clereg = clereg;
    }

    public short getCodereg() {
        return codereg;
    }

    public void setCodereg(short codereg) {
        this.codereg = codereg;
    }

    public String getNomReg() {
        return nomReg;
    }

    public void setNomReg(String nomReg) {
        this.nomReg = nomReg;
    }

    @XmlTransient
    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clereg != null ? clereg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Region)) {
            return false;
        }
        Region other = (Region) object;
        if ((this.clereg == null && other.clereg != null) || (this.clereg != null && !this.clereg.equals(other.clereg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Region[ clereg=" + clereg + " ]";
    }
    
}
