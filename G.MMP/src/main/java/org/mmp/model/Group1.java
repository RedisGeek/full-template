/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "group")

public class Group1 {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODEGROUP")
    private Short codegroup;
    @Column(name = "CLEUTIL")
    private Short cleutil;
    @Column(name = "NUMGROUP")
    private Short numgroup;
    @Size(max = 128)
    @Column(name = "NOMGROUP")
    private String nomgroup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUT")
    private int statut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NOMSTATUT")
    private String nomstatut;

    public Group1() {
    }

    public Group1(Short codegroup) {
        this.codegroup = codegroup;
    }

    public Group1(Short codegroup, int statut, String nomstatut) {
        this.codegroup = codegroup;
        this.statut = statut;
        this.nomstatut = nomstatut;
    }

    public Short getCodegroup() {
        return codegroup;
    }

    public void setCodegroup(Short codegroup) {
        this.codegroup = codegroup;
    }

    public Short getCleutil() {
        return cleutil;
    }

    public void setCleutil(Short cleutil) {
        this.cleutil = cleutil;
    }

    public Short getNumgroup() {
        return numgroup;
    }

    public void setNumgroup(Short numgroup) {
        this.numgroup = numgroup;
    }

    public String getNomgroup() {
        return nomgroup;
    }

    public void setNomgroup(String nomgroup) {
        this.nomgroup = nomgroup;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public String getNomstatut() {
        return nomstatut;
    }

    public void setNomstatut(String nomstatut) {
        this.nomstatut = nomstatut;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codegroup != null ? codegroup.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Group1)) {
            return false;
        }
        Group1 other = (Group1) object;
        if ((this.codegroup == null && other.codegroup != null) || (this.codegroup != null && !this.codegroup.equals(other.codegroup))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Group1[ codegroup=" + codegroup + " ]";
    }
    
}
