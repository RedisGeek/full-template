package org.mmp.model;

import java.util.Date;

public class Operateurs {
    private Integer codeexpl;
    private Short codetyp;
    private String nomexploitant;
    private String prenomexpl;
    private String datenaiss;
    private String sexe;
    private String age;
    private String adresse;
    private String numcin;
    private String datecin;
    private String lieucin;
    private String numtel;
    private String nif;
    private int valide;
    private Integer fokotany;
    private Integer banque;
    private String stat;
    private String raisonsocial;

    public Integer getCodeexpl() {
        return codeexpl;
    }

    public void setCodeexpl(Integer codeexpl) {
        this.codeexpl = codeexpl;
    }

    public Short getCodetyp() {
        return codetyp;
    }

    public void setCodetyp(Short codetyp) {
        this.codetyp = codetyp;
    }

    public String getNomexploitant() {
        return nomexploitant;
    }

    public void setNomexploitant(String nomexploitant) {
        this.nomexploitant = nomexploitant;
    }

    public String getPrenomexpl() {
        return prenomexpl;
    }

    public void setPrenomexpl(String prenomexpl) {
        this.prenomexpl = prenomexpl;
    }

    public String getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(String datenaiss) {
        this.datenaiss = datenaiss;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumcin() {
        return numcin;
    }

    public void setNumcin(String numcin) {
        this.numcin = numcin;
    }

    public String getDatecin() {
        return datecin;
    }

    public void setDatecin(String datecin) {
        this.datecin = datecin;
    }

    public String getLieucin() {
        return lieucin;
    }

    public void setLieucin(String lieucin) {
        this.lieucin = lieucin;
    }

    public String getNumtel() {
        return numtel;
    }

    public void setNumtel(String numtel) {
        this.numtel = numtel;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public int getValide() {
        return valide;
    }

    public void setValide(int valide) {
        this.valide = valide;
    }

    public Integer getFokotany() {
        return fokotany;
    }

    public void setFokotany(Integer fokotany) {
        this.fokotany = fokotany;
    }

    public Integer getBanque() {
        return banque;
    }

    public void setBanque(Integer banque) {
        this.banque = banque;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getRaisonsocial() {
        return raisonsocial;
    }

    public void setRaisonsocial(String raisonsocial) {
        this.raisonsocial = raisonsocial;
    }
}
