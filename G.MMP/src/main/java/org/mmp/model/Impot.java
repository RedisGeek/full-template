/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "impot")
public class Impot implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIMP")
    private Short codimp;
    @Size(max = 128)
    @Column(name = "DESIGNATION_IMPOT")
    private String designationImpot;
    @Size(max = 128)
    @Column(name = "OBSERVATION")
    private String observation;

    public Impot() {
    }

    public Impot(Short codimp) {
        this.codimp = codimp;
    }

    public Short getCodimp() {
        return codimp;
    }

    public void setCodimp(Short codimp) {
        this.codimp = codimp;
    }

    public String getDesignationImpot() {
        return designationImpot;
    }

    public void setDesignationImpot(String designationImpot) {
        this.designationImpot = designationImpot;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codimp != null ? codimp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Impot)) {
            return false;
        }
        Impot other = (Impot) object;
        if ((this.codimp == null && other.codimp != null) || (this.codimp != null && !this.codimp.equals(other.codimp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Impot[ codimp=" + codimp + " ]";
    }
    
}
