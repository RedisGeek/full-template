/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "logs_utilisateur")
public class LogsUtilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLELOG")
    private Short clelog;
    @Column(name = "CLEUTIL")
    private Short cleutil;
    @Column(name = "DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ACTION")
    private String action;

    public LogsUtilisateur() {
    }

    public LogsUtilisateur(Short clelog) {
        this.clelog = clelog;
    }

    public LogsUtilisateur(Short clelog, String action) {
        this.clelog = clelog;
        this.action = action;
    }

    public Short getClelog() {
        return clelog;
    }

    public void setClelog(Short clelog) {
        this.clelog = clelog;
    }

    public Short getCleutil() {
        return cleutil;
    }

    public void setCleutil(Short cleutil) {
        this.cleutil = cleutil;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clelog != null ? clelog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogsUtilisateur)) {
            return false;
        }
        LogsUtilisateur other = (LogsUtilisateur) object;
        if ((this.clelog == null && other.clelog != null) || (this.clelog != null && !this.clelog.equals(other.clelog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.LogsUtilisateur[ clelog=" + clelog + " ]";
    }
    
}
