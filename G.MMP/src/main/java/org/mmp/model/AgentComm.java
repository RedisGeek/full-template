/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mmp.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "agent_comm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgentComm.findAll", query = "SELECT a FROM AgentComm a"),
    @NamedQuery(name = "AgentComm.findByCodagcom", query = "SELECT a FROM AgentComm a WHERE a.codagcom = :codagcom"),
    @NamedQuery(name = "AgentComm.findByNomagcom", query = "SELECT a FROM AgentComm a WHERE a.nomagcom = :nomagcom"),
    @NamedQuery(name = "AgentComm.findByCodecom", query = "SELECT a FROM AgentComm a WHERE a.codecom = :codecom"),
    @NamedQuery(name = "AgentComm.findByCin", query = "SELECT a FROM AgentComm a WHERE a.cin = :cin"),
    @NamedQuery(name = "AgentComm.findByFonction", query = "SELECT a FROM AgentComm a WHERE a.fonction = :fonction")})
public class AgentComm implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODAGCOM")
    private Short codagcom;
    @Size(max = 128)
    @Column(name = "NOMAGCOM")
    private String nomagcom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODECOM")
    private int codecom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "CIN")
    private String cin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FONCTION")
    private int fonction;
    @OneToMany(mappedBy = "codagcom")
    private List<Commune> communeList;

    public AgentComm() {
    }

    public AgentComm(Short codagcom) {
        this.codagcom = codagcom;
    }

    public AgentComm(Short codagcom, int codecom, String cin, int fonction) {
        this.codagcom = codagcom;
        this.codecom = codecom;
        this.cin = cin;
        this.fonction = fonction;
    }

    public Short getCodagcom() {
        return codagcom;
    }

    public void setCodagcom(Short codagcom) {
        this.codagcom = codagcom;
    }

    public String getNomagcom() {
        return nomagcom;
    }

    public void setNomagcom(String nomagcom) {
        this.nomagcom = nomagcom;
    }

    public int getCodecom() {
        return codecom;
    }

    public void setCodecom(int codecom) {
        this.codecom = codecom;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public int getFonction() {
        return fonction;
    }

    public void setFonction(int fonction) {
        this.fonction = fonction;
    }

    @XmlTransient
    public List<Commune> getCommuneList() {
        return communeList;
    }

    public void setCommuneList(List<Commune> communeList) {
        this.communeList = communeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codagcom != null ? codagcom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentComm)) {
            return false;
        }
        AgentComm other = (AgentComm) object;
        if ((this.codagcom == null && other.codagcom != null) || (this.codagcom != null && !this.codagcom.equals(other.codagcom))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.AgentComm[ codagcom=" + codagcom + " ]";
    }
    
}
